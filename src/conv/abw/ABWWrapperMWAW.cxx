/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "ABWWrapperMWAW.hxx"
#include "WPWrapperMWAW.hxx"

#include <librevenge/librevenge.h>
#include <librvngabw/librvngabw.hxx>
#include <libmwaw/libmwaw.hxx>

#include "UserOptions.hxx"

namespace writerperfectrvngabw
{
/// try to convert embedded picture in svg.
static bool handleEmbeddedBasicGraphicImage(const librevenge::RVNGBinaryData &data, librevenge::RVNGBinaryData &output)
{
	librevenge::RVNGStringVector svgOutput;
	librevenge::RVNGSVGDrawingGenerator generator(svgOutput, "");
	return MWAWDocument::MWAW_R_OK == MWAWDocument::parse(data.getDataStream(), &generator) &&
	       ABWWrapper::convertSVGImage(svgOutput, output);
}
//! try to convert MWAW created picture in svg
static bool handleEmbeddedMWAWGraphicImage(const librevenge::RVNGBinaryData &data, librevenge::RVNGBinaryData &output)
{
	librevenge::RVNGStringVector svgOutput;
	librevenge::RVNGSVGDrawingGenerator generator(svgOutput, "");
	return MWAWDocument::decodeGraphic(data, &generator) &&
	       ABWWrapper::convertSVGImage(svgOutput, output);
}
//! try to convert MWAW created spreadsheet in a table
static bool handleEmbeddedMWAWSpreadsheetObject(const librevenge::RVNGBinaryData &data, librvngabw::ABWGenerator &generator)
{
	librvngabw::ABWSpreadsheetToTableGenerator exporter(generator);
	return MWAWDocument::decodeSpreadsheet(data, &exporter);
}

ABWWrapperMWAW::ABWWrapperMWAW(shared_ptr<UserOptions> options) :
	ABWWrapper(shared_ptr<writerperfect::WPWrapper>(new writerperfect::WPWrapperMWAW(options)))
{
}
bool ABWWrapperMWAW::parseDocument(librevenge::RVNGInputStream &input, librvngabw::ABWTextGenerator &collector)
{
	// embedded document created by libmwaw
	collector.registerEmbeddedImageHandler("image/mwaw-odg", &handleEmbeddedMWAWGraphicImage);
	collector.registerEmbeddedObjectHandler("image/mwaw-ods", &handleEmbeddedMWAWSpreadsheetObject);
	// basic image
	collector.registerEmbeddedImageHandler("image/pict", &handleEmbeddedBasicGraphicImage);
	collector.registerEmbeddedImageHandler("image/x-pict", &handleEmbeddedBasicGraphicImage);
	return ABWWrapper::parseDocument(input, collector);
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
