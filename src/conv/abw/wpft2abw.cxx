/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>

#include "ABWConverter.hxx"
#include "ABWWrapper.hxx"
#include "UserOptions.hxx"

#define TOOLNAME "wpft2abw"

#ifdef ENABLE_LIBEBOOK
#  include "WPWrapperEBOOK.hxx"
#endif
#ifdef ENABLE_LIBETONYEK
#  include "WPWrapperETONYEK.hxx"
#endif
#ifdef ENABLE_LIBMWAW
#  include "ABWWrapperMWAW.hxx"
#endif
#ifdef ENABLE_LIBSTAROFFICE
#  include "ABWWrapperSTAROFFICE.hxx"
#endif
#ifdef ENABLE_LIBWPD
#  include "ABWWrapperWPD.hxx"
#endif
#ifdef ENABLE_LIBWPS
#  include "ABWWrapperWPS.hxx"
#endif

#define CHECK_CONVERTER_LIB(x)															\
	do {																				\
		if (!wpWrapper && !wrapper) {												    \
            shared_ptr<writerperfect::WPWrapper> testWrapper(new writerperfect::WPWrapper##x(options)); \
			if (converter.isSupportedFormat(*testWrapper,input))						\
				wpWrapper=testWrapper;													\
		}																				\
	} while(0)

#define CHECK_CONVERTER_LIB2(x)											\
	do {																				\
		if (!wpWrapper && !wrapper) {												    \
			shared_ptr<ABWWrapper> testWrapper(new ABWWrapper##x(options)); 			\
			if (converter.isSupportedFormat(*testWrapper,input))						\
				wrapper=testWrapper;													\
		}																				\
	} while(0)

using namespace writerperfectrvngabw;
int main(int argc, char *argv[])
{
	shared_ptr<UserOptions> options(new UserOptions);
	options->m_encodingOption=options->m_passwordOption=options->m_standartOuputOption=true;
	int exitCode;
	ABWConverter converter(options, TOOLNAME, "converts documents to AbiWord.");
	if (converter.parseAndTreatOptions(argc, argv, exitCode))
		return exitCode;

	shared_ptr<librevenge::RVNGInputStream> input(new librevenge::RVNGFileStream(options->getInput()));
	shared_ptr<writerperfect::WPWrapper> wpWrapper;
	shared_ptr<ABWWrapper> wrapper;
#ifdef ENABLE_LIBEBOOK
	CHECK_CONVERTER_LIB(EBOOK);
#endif
#ifdef ENABLE_LIBETONYEK
	CHECK_CONVERTER_LIB(ETONYEK);
#endif
#ifdef ENABLE_LIBMWAW
	CHECK_CONVERTER_LIB2(MWAW);
#endif
#ifdef ENABLE_LIBSTAROFFICE
	CHECK_CONVERTER_LIB2(STAROFFICE);
#endif
#ifdef ENABLE_LIBWPD
	CHECK_CONVERTER_LIB2(WPD);
#endif
#ifdef ENABLE_LIBWPS
	CHECK_CONVERTER_LIB2(WPS);
#endif
	if (wpWrapper && converter.convertDocument(*wpWrapper, *input))
		return 0;
	if (wrapper && converter.convertDocument(*wrapper, *input))
		return 0;
	fprintf(stderr, "ERROR : Couldn't convert the document\n");
	return 1;
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
