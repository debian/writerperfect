/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include <string.h>

#include "ABWWrapper.hxx"

namespace writerperfectrvngabw
{
writerperfect::WPWrapper::Type ABWWrapper::getType() const
{
	if (!m_wrapper) return writerperfect::WPWrapper::Unknown;
	return m_wrapper->m_type;
}
bool ABWWrapper::isErrorSent() const
{
	if (!m_wrapper) return false;
	return m_wrapper->m_errorSent;
}
bool ABWWrapper::checkInput(shared_ptr<librevenge::RVNGInputStream> &input)
{
	if (m_wrapper)
		return m_wrapper->checkInput(input);
	return false;
}
bool ABWWrapper::parseDocument(librevenge::RVNGInputStream &input, librvngabw::ABWTextGenerator &collector)
{
	if (m_wrapper)
		return m_wrapper->parseDocument(input, collector);
	return false;
}
bool ABWWrapper::convertSVGImage(librevenge::RVNGStringVector const &svgOutput, librevenge::RVNGBinaryData &output)
{
	if (svgOutput.empty() || svgOutput[0].empty())
		return false;
	output.clear();
	const char *svgHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
	                        "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\""
	                        " \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";
	output.append((unsigned char *)svgHeader, strlen(svgHeader));
	output.append((unsigned char *)svgOutput[0].cstr(), strlen(svgOutput[0].cstr()));
	return true;
}
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
