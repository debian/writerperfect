/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "ABWWrapperSTAROFFICE.hxx"

#include "WPWrapperSTAROFFICE.hxx"

#include <librevenge/librevenge.h>
#include <librvngabw/librvngabw.hxx>
#include <libstaroffice/libstaroffice.hxx>

#include "UserOptions.hxx"

namespace writerperfectrvngabw
{
//! try to convert embedded picture in svg.
static bool handleEmbeddedBasicGraphicImage(const librevenge::RVNGBinaryData &data, librevenge::RVNGBinaryData &output)
{
	librevenge::RVNGStringVector svgOutput;
	librevenge::RVNGSVGDrawingGenerator generator(svgOutput, "");
	return STOFFDocument::STOFF_R_OK == STOFFDocument::parse(data.getDataStream(), &generator) &&
	       ABWWrapper::convertSVGImage(svgOutput, output);
}
//! try to convert STOFF created picture in svg
static bool handleEmbeddedSTOFFGraphicImage(const librevenge::RVNGBinaryData &data, librevenge::RVNGBinaryData &output)
{
	librevenge::RVNGStringVector svgOutput;
	librevenge::RVNGSVGDrawingGenerator generator(svgOutput, "");
	return STOFFDocument::decodeGraphic(data, &generator) &&
	       ABWWrapper::convertSVGImage(svgOutput, output);
}
//! try to convert STOFF created spreadsheet in a table
static bool handleEmbeddedSTOFFSpreadsheetObject(const librevenge::RVNGBinaryData &data, librvngabw::ABWGenerator &generator)
{
	librvngabw::ABWSpreadsheetToTableGenerator exporter(generator);
	return STOFFDocument::decodeSpreadsheet(data, &exporter);
}

ABWWrapperSTAROFFICE::ABWWrapperSTAROFFICE(shared_ptr<UserOptions> options) :
	ABWWrapper(shared_ptr<writerperfect::WPWrapper>(new writerperfect::WPWrapperSTAROFFICE(options)))
{
}
bool ABWWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, librvngabw::ABWTextGenerator &collector)
{
	collector.registerEmbeddedImageHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicImage);
	collector.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	// basic image
	collector.registerEmbeddedImageHandler("image/pict", &handleEmbeddedBasicGraphicImage);
	return ABWWrapper::parseDocument(input, collector);
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
