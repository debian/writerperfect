/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "ABWWrapperWPS.hxx"

#include "WPWrapperWPS.hxx"

#include <librevenge/librevenge.h>
#include <librvngabw/librvngabw.hxx>
#include <libwps/libwps.h>

#include "UserOptions.hxx"

namespace writerperfectrvngabw
{
static bool handleEmbeddedWKSObject(const librevenge::RVNGBinaryData &data, librvngabw::ABWGenerator &generator)
{
	if (!data.size()) return false;
	librvngabw::ABWSpreadsheetToTableGenerator exporter(generator);
	return libwps::WPSDocument::parse(data.getDataStream(), &exporter)==libwps::WPS_OK;
}

ABWWrapperWPS::ABWWrapperWPS(shared_ptr<UserOptions> options) :
	ABWWrapper(shared_ptr<writerperfect::WPWrapper>(new writerperfect::WPWrapperWPS(options)))
{
}
bool ABWWrapperWPS::parseDocument(librevenge::RVNGInputStream &input, librvngabw::ABWTextGenerator &collector)
{
	collector.registerEmbeddedObjectHandler("image/wks-ods", &handleEmbeddedWKSObject);
	return ABWWrapper::parseDocument(input, collector);
}

}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
