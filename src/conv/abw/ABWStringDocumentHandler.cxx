/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include <stdio.h>
#include <string.h>

#include "ABWStringDocumentHandler.hxx"
#include "StringXMLSerializer.hxx"

namespace writerperfectrvngabw
{

class ABWStringDocumentHandlerPrivate
{
public:
	ABWStringDocumentHandlerPrivate() : m_serializer()
	{
	}

	StringXMLSerializer m_serializer;
};

ABWStringDocumentHandler::ABWStringDocumentHandler() : m_data(new ABWStringDocumentHandlerPrivate)
{
	m_data->m_serializer.setDocType("<!DOCTYPE abiword PUBLIC \"-//ABISOURCE//DTD AWML 1.0 Strict//EN\" \"http://www.abisource.com/awml.dtd\">");
}

ABWStringDocumentHandler::~ABWStringDocumentHandler()
{
}

librevenge::RVNGString const &ABWStringDocumentHandler::getData() const
{
	return m_data->m_serializer.getData();
}

bool ABWStringDocumentHandler::write(char const *output) const
{
	if (output && strlen(output))
	{
		FILE *fhandle = fopen(output, "wb");
		if (!fhandle)
			return false;
		fprintf(fhandle, "%s\n", getData().cstr());
		fclose(fhandle);
	}
	else
		printf("%s\n", getData().cstr());
	return true;
}

void ABWStringDocumentHandler::startDocument()
{
	m_data->m_serializer.startDocument();
}

void ABWStringDocumentHandler::endDocument()
{
	m_data->m_serializer.endDocument();
}

void ABWStringDocumentHandler::startElement(const char *psName, const librevenge::RVNGPropertyList &xPropList)
{
	librevenge::RVNGPropertyList propList;
	librevenge::RVNGPropertyList::Iter i(xPropList);
	for (i.rewind(); i.next();)
	{
		librevenge::RVNGString sEscapedCharacters("");
		const librevenge::RVNGPropertyListVector *child=i.child();
		if (!child)
		{
			if (i()->getStr().len()>0)
				sEscapedCharacters.appendEscapedXML(i()->getStr());
		}
		else if (child->count()>0)
		{
			librevenge::RVNGString data("");
			librevenge::RVNGPropertyList::Iter cIt((*child)[0]);
			bool first=true;
			for (cIt.rewind(); cIt.next();)
			{
				if (!first) data.append("; ");
				first=false;
				data.append(cIt.key());
				if (cIt()->getStr().len()>0)   // checkme: we need also to escape ":;" here
				{
					data.append(":");
					data.append(cIt()->getStr());
				}
			}
			sEscapedCharacters.appendEscapedXML(data);
		}

		propList.insert(i.key(), sEscapedCharacters);
	}

	m_data->m_serializer.startElement(psName, propList);
}

void ABWStringDocumentHandler::endElement(const char *psName)
{
	m_data->m_serializer.endElement(psName);
}

void ABWStringDocumentHandler::characters(const librevenge::RVNGString &sCharacters)
{
	m_data->m_serializer.characters(sCharacters);
}
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
