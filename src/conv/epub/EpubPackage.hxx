/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef INCLUDED_EPUBPACKAGE_H
#define INCLUDED_EPUBPACKAGE_H

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <libepubgen/libepubgen.h>

#include "OutputFileHelper.hxx"
#include "writerperfect_utils.hxx"

class StringXMLSerializer;

class EpubPackage : public libepubgen::EPUBPackage
{
	typedef std::vector<std::pair<librevenge::RVNGString, librevenge::RVNGPropertyList> > CSSContent_t;

public:
	explicit EpubPackage(const std::string &fileName);

	virtual ~EpubPackage();

	virtual void openBinaryFile(const char *name);
	virtual void closeBinaryFile();

	virtual void openCSSFile(const char *name);
	virtual void closeCSSFile();

	virtual void openXMLFile(const char *name);
	virtual void closeXMLFile();

	virtual void openElement(const char *name, const librevenge::RVNGPropertyList &attributes);
	virtual void closeElement(const char *name);

	virtual void insertRule(const librevenge::RVNGString &selector, const librevenge::RVNGPropertyList &properties);

	virtual void insertCharacters(const librevenge::RVNGString &characters);

	virtual void insertBinaryData(const librevenge::RVNGBinaryData &data);

	virtual void openTextFile(const char *name);

	virtual void insertText(const librevenge::RVNGString &text);
	virtual void insertLineBreak();

	virtual void closeTextFile();

private:
	void checkNoOpenedFile() const;

private:
	OutputFileHelper m_output;
	std::string m_currentFileName;
	std::vector<unsigned char> m_currentBinaryContent;
	CSSContent_t m_currentCSSContent;
	shared_ptr<StringXMLSerializer> m_currentXMLContent;
	std::ostringstream m_currentTextContent;

};

#endif // INCLUDED_EPUBPACKAGE_H

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
