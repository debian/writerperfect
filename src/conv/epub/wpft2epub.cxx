/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>

#include <libepubgen/libepubgen.h>

#include "EpubConverter.hxx"
#include "UserOptions.hxx"

#define TOOLNAME "wpft2epub"

#ifdef ENABLE_LIBABW
#  include "WPWrapperABW.hxx"
#endif
#ifdef ENABLE_LIBCDR
#  include "WPWrapperCDR.hxx"
#endif
#ifdef ENABLE_LIBEBOOK
#  include "WPWrapperEBOOK.hxx"
#endif
#ifdef ENABLE_LIBETONYEK
#  include "WPWrapperETONYEK.hxx"
#endif
#ifdef ENABLE_LIBFREEHAND
#  include "WPWrapperFREEHAND.hxx"
#endif
#ifdef ENABLE_LIBMSPUB
#  include "WPWrapperMSPUB.hxx"
#endif
#ifdef ENABLE_LIBMWAW
#  include "WPWrapperMWAW.hxx"
#endif
#ifdef ENABLE_LIBPAGEMAKER
#  include "WPWrapperPAGEMAKER.hxx"
#endif
#ifdef ENABLE_LIBSTAROFFICE
#  include "WPWrapperSTAROFFICE.hxx"
#endif
#ifdef ENABLE_LIBVISIO
#  include "WPWrapperVISIO.hxx"
#endif
#ifdef ENABLE_LIBWPD
#  include "WPWrapperWPD.hxx"
#endif
#ifdef ENABLE_LIBWPG
#  include "WPWrapperWPG.hxx"
#endif
#ifdef ENABLE_LIBWPS
#  include "WPWrapperWPS.hxx"
#endif
#ifdef ENABLE_LIBZMF
#  include "WPWrapperZMF.hxx"
#endif

#define CHECK_CONVERTER_LIB(x)															\
	do {																				\
		if (!wrapper) {																	\
			shared_ptr<WPWrapper> testWrapper(new WPWrapper##x(options));				\
			if (testWrapper && testWrapper->checkInput(input))							\
				wrapper=testWrapper;													\
		}																				\
	} while(0)

using namespace writerperfect;
int main(int argc, char *argv[])
{
	shared_ptr<UserOptions> options(new UserOptions);
	options->m_encodingOption=options->m_passwordOption=true;
	int exitCode;
	writerperfectrvngepub::EpubConverter converter(options, TOOLNAME, "converts documents to ePub.");
	if (converter.parseAndTreatOptions(argc, argv, exitCode))
		return exitCode;

	shared_ptr<librevenge::RVNGInputStream> input(new librevenge::RVNGFileStream(options->getInput()));
	shared_ptr<WPWrapper> wrapper;

#ifdef ENABLE_LIBABW
	CHECK_CONVERTER_LIB(ABW);
#endif
#ifdef ENABLE_LIBCDR
	CHECK_CONVERTER_LIB(CDR);
#endif
#ifdef ENABLE_LIBEBOOK
	CHECK_CONVERTER_LIB(EBOOK);
#endif
#ifdef ENABLE_LIBETONYEK
	CHECK_CONVERTER_LIB(ETONYEK);
#endif
#ifdef ENABLE_LIBFREEHAND
	CHECK_CONVERTER_LIB(FREEHAND);
#endif
#ifdef ENABLE_LIBMSPUB
	CHECK_CONVERTER_LIB(MSPUB);
#endif
#ifdef ENABLE_LIBMWAW
	CHECK_CONVERTER_LIB(MWAW);
#endif
#ifdef ENABLE_LIBPAGEMAKER
	CHECK_CONVERTER_LIB(PAGEMAKER);
#endif
#ifdef ENABLE_LIBSTAROFFICE
	CHECK_CONVERTER_LIB(STAROFFICE);
#endif
#ifdef ENABLE_LIBVISIO
	CHECK_CONVERTER_LIB(VISIO);
#endif
#ifdef ENABLE_LIBWPD
	CHECK_CONVERTER_LIB(WPD);
#endif
#ifdef ENABLE_LIBWPG
	CHECK_CONVERTER_LIB(WPG);
#endif
#ifdef ENABLE_LIBWPS
	CHECK_CONVERTER_LIB(WPS);
#endif
#ifdef ENABLE_LIBZMF
	CHECK_CONVERTER_LIB(ZMF);
#endif

	if (!wrapper || !converter.convertDocument(*wrapper, *input))
	{
		fprintf(stderr, "ERROR : Couldn't convert the document\n");
		return 1;
	}

	return 0;
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
