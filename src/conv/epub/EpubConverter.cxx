/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include <stdio.h>
#include <iostream>

#include "EpubConverter.hxx"

#include "EpubPackage.hxx"
#include "UsageHelper.hxx"
#include "UserOptions.hxx"
#include "WPWrapper.hxx"

namespace writerperfectrvngepub
{
EpubConverter::EpubConverter(shared_ptr<UserOptions> options, std::string const &toolName, std::string const &usageMessage) :
	m_options(options), m_toolName(toolName), m_usageMessage(usageMessage)
{
#if defined HAVE_EPUBGEN_0_1
	options->m_versionOption = true;
#endif
	options->m_splitOption = true;
	options->m_stylesOption = true;
	options->m_layoutOption = true;
}

EpubConverter::~EpubConverter()
{
}

int EpubConverter::printUsage() const
{
	if (m_toolName.empty() || m_usageMessage.empty())
	{
		fprintf(stderr, "INTERNAL ERROR: not enough data to show help.\n");
		return 1;
	}
	UsageHelper usage(m_toolName.c_str(), m_usageMessage.c_str(), "[OPTIONS] INPUT OUTPUT");
	if (m_options) m_options->addOptionsToUsageHelper(usage);
	return usage.printUsage();
}

bool EpubConverter::parseAndTreatOptions(int argc, char const *const *argv, int &exitCode) const
{
	if (!m_options)
	{
		fprintf(stderr, "INTERNAL ERROR: can not find the options.\n");
		exitCode=1;
	}
	else if (!m_options->parseOptions(argc, argv) || m_options->m_showHelp)
		exitCode=printUsage();
	else if (m_options->m_showListEncoding)
		exitCode=UsageHelper::printEncodings();
	else if (m_options->m_showVersion)
		exitCode=UsageHelper::printVersion(m_toolName.c_str());
	else if (!m_options->getInput() || !m_options->getOutput())
		exitCode=printUsage();
	else
		return false;
	return true;
}

bool EpubConverter::convertDocument(writerperfect::WPWrapper &wrapper, librevenge::RVNGInputStream &input)
{
	if (!m_options || !m_options->getOutput())
		return false;
	if (wrapper.m_type==writerperfect::WPWrapper::Drawing)
	{
		EpubPackage package(m_options->getOutput());
		libepubgen::EPUBDrawingGenerator collector(&package);
		return wrapper.parseDocument(input, collector);
	}
	if (wrapper.m_type==writerperfect::WPWrapper::Presentation)
	{
		EpubPackage package(m_options->getOutput());
		libepubgen::EPUBPresentationGenerator collector(&package);
		return wrapper.parseDocument(input, collector);
	}
	if (wrapper.m_type==writerperfect::WPWrapper::Text)
	{
		EpubPackage package(m_options->getOutput());
#if defined HAVE_EPUBGEN_0_1
		libepubgen::EPUBTextGenerator collector(&package, m_options->getVersion());
		collector.setOption(libepubgen::EPUB_GENERATOR_OPTION_SPLIT, m_options->getSplit());
		collector.setOption(libepubgen::EPUB_GENERATOR_OPTION_STYLES, m_options->getStyles());
		collector.setOption(libepubgen::EPUB_GENERATOR_OPTION_LAYOUT, m_options->getLayout());
#else
		libepubgen::EPUBTextGenerator collector(&package, static_cast<libepubgen::EPUBSplitMethod>(m_options->getSplit()));
#endif
		return wrapper.parseDocument(input, collector);
	}
	return false;
}

bool EpubConverter::isSupportedFormat(writerperfect::WPWrapper &wrapper, shared_ptr<librevenge::RVNGInputStream> &input)
{
	if (!m_options)
		return false;
	if (!wrapper.checkInput(input))
		return false;
	return wrapper.m_type==writerperfect::WPWrapper::Drawing || wrapper.m_type==writerperfect::WPWrapper::Presentation ||
	       wrapper.m_type==writerperfect::WPWrapper::Text;
}

int EpubConverter::convert(writerperfect::WPWrapper &wrapper, int argc, char const *const *argv)
{
	int exitCode;
	if (parseAndTreatOptions(argc, argv, exitCode))
		return exitCode;

	shared_ptr<librevenge::RVNGInputStream> input(new librevenge::RVNGFileStream(m_options->getInput()));
	if (!isSupportedFormat(wrapper, input))
	{
		if (!wrapper.m_errorSent)
			fprintf(stderr, "ERROR: We have no confidence that you are giving us a valid document.\n");
		return 1;
	}

	if (!input || !convertDocument(wrapper, *input))
	{
		if (!wrapper.m_errorSent)
			fprintf(stderr, "ERROR : Couldn't convert the document\n");
		return 1;
	}

	return 0;
}
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
