/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* libwpd
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef EPUB_CONVERTER_H
#define EPUB_CONVERTER_H

#include <string>

#include <librevenge/librevenge.h>

#include "writerperfect_utils.hxx"

class UserOptions;

namespace writerperfect
{
class WPWrapper;
}

namespace writerperfectrvngepub
{
/** a abstract class to help convert file
 */
class EpubConverter
{
public:
	//! constructor
	EpubConverter(shared_ptr<UserOptions> options, std::string const &toolName, std::string const &usageMessage);
	//! destructor
	~EpubConverter();

	/** try to parse/treat the argument, to do the conversion, ... Returns the exit code */
	int convert(writerperfect::WPWrapper &wrapper, int argc, char const *const *argv);

	// low level function

	/** print the options and try to call basic functions:
		- printVersion, printUsage: if toolName and usage message is set
		- printEncoding
		Return true and set the exit code if the program can exit
	 */
	bool parseAndTreatOptions(int argc, char const *const *argv, int &exitCode) const;
	/// check if a file is supported
	bool isSupportedFormat(writerperfect::WPWrapper &wrapper, shared_ptr<librevenge::RVNGInputStream> &input);
	/// try to convert a file
	bool convertDocument(writerperfect::WPWrapper &wrapper, librevenge::RVNGInputStream &input);
	/// print usage: toolName and usage message must be set
	int printUsage() const;
protected:
	//! the option
	shared_ptr<UserOptions> m_options;
	//! the tool name
	std::string m_toolName;
	//! the usage message
	std::string m_usageMessage;
};
}
#endif

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
