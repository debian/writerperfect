/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include <cassert>
#include <sstream>

#include "EpubPackage.hxx"
#include "StringXMLSerializer.hxx"

EpubPackage::EpubPackage(const std::string &fileName)
	: m_output(fileName.c_str())
	, m_currentFileName()
	, m_currentBinaryContent()
	, m_currentXMLContent()
{
	m_output.writeChildFile("mimetype", "application/epub+zip", 0);
}

EpubPackage::~EpubPackage()
{
	checkNoOpenedFile();
}

void EpubPackage::openBinaryFile(const char *const name)
{
	checkNoOpenedFile();
	assert(name);

	m_currentFileName = name;
}

void EpubPackage::closeBinaryFile()
{
	assert(!m_currentFileName.empty());

	if (m_currentBinaryContent.empty())
		m_output.writeChildFile(m_currentFileName.c_str(), "");
	else
		m_output.writeChildFile(m_currentFileName.c_str(), &m_currentBinaryContent[0], m_currentBinaryContent.size());

	m_currentFileName.clear();
	m_currentBinaryContent.clear();
}

void EpubPackage::openCSSFile(const char *const name)
{
	checkNoOpenedFile();
	assert(name);

	m_currentFileName = name;
}

void EpubPackage::closeCSSFile()
{
	assert(!m_currentFileName.empty());

	std::ostringstream out;

	bool first = true;
	for (CSSContent_t::const_iterator it = m_currentCSSContent.begin(); m_currentCSSContent.end() != it; ++it)
	{
		if (!first)
			out << '\n';
		else
			first = false;

		out << it->first.cstr() << " {\n";
		for (librevenge::RVNGPropertyList::Iter propIt(it->second); !propIt.last(); propIt.next())
		{
			if (propIt())
				out << "  " << propIt.key() << ": " << propIt()->getStr().cstr() << ";\n";
		}
		out << "}\n";
	}

	m_output.writeChildFile(m_currentFileName.c_str(), out.str().c_str());

	m_currentFileName.clear();
	m_currentCSSContent.clear();
}

void EpubPackage::openXMLFile(const char *const name)
{
	checkNoOpenedFile();
	assert(name);

	m_currentFileName = name;
	m_currentXMLContent.reset(new StringXMLSerializer());

	m_currentXMLContent->startDocument();
}

void EpubPackage::closeXMLFile()
{
	assert(!m_currentFileName.empty());

	m_currentXMLContent->endDocument();

	m_output.writeChildFile(m_currentFileName.c_str(), m_currentXMLContent->getData().cstr());

	m_currentFileName.clear();
	m_currentXMLContent.reset();
}

void EpubPackage::openElement(const char *const name, const librevenge::RVNGPropertyList &attributes)
{
	assert(!m_currentFileName.empty());
	assert(bool(m_currentXMLContent));

	m_currentXMLContent->startElement(name, attributes);
}

void EpubPackage::closeElement(const char *const name)
{
	assert(!m_currentFileName.empty());
	assert(bool(m_currentXMLContent));

	m_currentXMLContent->endElement(name);
}

void EpubPackage::insertCharacters(const librevenge::RVNGString &characters)
{
	assert(!m_currentFileName.empty());
	assert(bool(m_currentXMLContent));

	m_currentXMLContent->characters(characters);
}

void EpubPackage::insertRule(const librevenge::RVNGString &selector, const librevenge::RVNGPropertyList &properties)
{
	assert(!m_currentFileName.empty());

	m_currentCSSContent.push_back(std::make_pair(selector, properties));
}

void EpubPackage::insertBinaryData(const librevenge::RVNGBinaryData &data)
{
	assert(!m_currentFileName.empty());

	if (!data.empty())
		m_currentBinaryContent.insert(m_currentBinaryContent.end(), data.getDataBuffer(), data.getDataBuffer() + data.size());
}

void EpubPackage::openTextFile(const char *const name)
{
	checkNoOpenedFile();
	assert(name);

	m_currentFileName = name;
}

void EpubPackage::insertText(const librevenge::RVNGString &text)
{
	assert(!m_currentFileName.empty());

	m_currentTextContent << text.cstr();
}

void EpubPackage::insertLineBreak()
{
	assert(!m_currentFileName.empty());

	m_currentTextContent << '\n';
}

void EpubPackage::closeTextFile()
{
	assert(!m_currentFileName.empty());

	m_output.writeChildFile(m_currentFileName.c_str(), m_currentTextContent.str().c_str());

	m_currentFileName.clear();
	m_currentTextContent.clear();
}

void EpubPackage::checkNoOpenedFile() const
{
	assert(m_currentFileName.empty());
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
