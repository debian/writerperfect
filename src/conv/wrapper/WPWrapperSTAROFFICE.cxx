/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "WPWrapperSTAROFFICE.hxx"

#include <librevenge/librevenge.h>
#include <libstaroffice/libstaroffice.hxx>

#include "UserOptions.hxx"

namespace writerperfect
{
bool WPWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGDrawingInterface &collector)
{
	return STOFFDocument::STOFF_R_OK == STOFFDocument::parse(&input, &collector, m_options->getPassword());
}
bool WPWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGPresentationInterface &collector)
{
	return STOFFDocument::STOFF_R_OK == STOFFDocument::parse(&input, &collector, m_options->getPassword());
}
bool WPWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGSpreadsheetInterface &collector)
{
	return STOFFDocument::STOFF_R_OK == STOFFDocument::parse(&input, &collector, m_options->getPassword());
}
bool WPWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGTextInterface &collector)
{
	return STOFFDocument::STOFF_R_OK == STOFFDocument::parse(&input, &collector, m_options->getPassword());
}
bool WPWrapperSTAROFFICE::checkInput(shared_ptr<librevenge::RVNGInputStream> &input)
{
	STOFFDocument::Kind kind;
	STOFFDocument::Confidence confidence = STOFFDocument::isFileFormatSupported(input.get(), kind);
	if (confidence != STOFFDocument::STOFF_C_EXCELLENT && confidence != STOFFDocument::STOFF_C_SUPPORTED_ENCRYPTION)
		return false;
	if (kind == STOFFDocument::STOFF_K_PRESENTATION)
		m_type=Presentation;
#if (STOFF_GRAPHIC_VERSION>=1)
	/* STOFF_GRAPHIC_VERSION>=1 indicates that:
	   - we begin converting .sda and .sdg files,
	   - and that STOFF_K_GRAPHIC is defined
	   so ok...
	 */
	else if (kind == STOFFDocument::STOFF_K_DRAW || kind == STOFFDocument::STOFF_K_GRAPHIC)
		m_type = Drawing;
#endif
	else if (kind == STOFFDocument::STOFF_K_SPREADSHEET)
		m_type=Spreadsheet;
	else if (kind == STOFFDocument::STOFF_K_TEXT)
		m_type=Text;
	else
		return false;
	return true;
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
