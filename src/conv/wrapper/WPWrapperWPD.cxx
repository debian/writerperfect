/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */
#include <stdio.h>

#include "WPWrapperWPD.hxx"

#include <librevenge/librevenge.h>
#include <libwpd/libwpd.h>
#include "UserOptions.hxx"

namespace writerperfect
{
bool WPWrapperWPD::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGTextInterface &collector)
{
	return libwpd::WPD_OK == libwpd::WPDocument::parse(&input, &collector, m_options->getPassword());
}
bool WPWrapperWPD::checkInput(shared_ptr<librevenge::RVNGInputStream> &input)
{
	libwpd::WPDConfidence confidence = libwpd::WPDocument::isFileFormatSupported(input.get());
	if (libwpd::WPD_CONFIDENCE_EXCELLENT == confidence)
	{
		m_type=Text;
		return true;
	}
	if (libwpd::WPD_CONFIDENCE_SUPPORTED_ENCRYPTION == confidence && m_options->getPassword() &&
	        libwpd::WPD_PASSWORD_MATCH_OK == libwpd::WPDocument::verifyPassword(input.get(), m_options->getPassword()))
	{
		m_type=Text;
		return true;
	}
	if (libwpd::WPD_CONFIDENCE_SUPPORTED_ENCRYPTION == confidence)
	{
		fprintf(stderr, "ERROR: The WordPerfect document is encrypted and we either\n");
		fprintf(stderr, "ERROR: don't know how to decrypt it or the given password is wrong.\n");
		m_errorSent=true;
	}
	return false;
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
