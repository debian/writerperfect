/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "WPWrapperETONYEK.hxx"

#include <librevenge/librevenge.h>
#include <libetonyek/libetonyek.h>

#include "UserOptions.hxx"

namespace writerperfect
{
bool WPWrapperETONYEK::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGPresentationInterface &collector)
{
	return libetonyek::EtonyekDocument::parse(&input, &collector);
}
bool WPWrapperETONYEK::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGSpreadsheetInterface &collector)
{
	return libetonyek::EtonyekDocument::parse(&input, &collector);
}
bool WPWrapperETONYEK::parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGTextInterface &collector)
{
	return libetonyek::EtonyekDocument::parse(&input, &collector);
}
bool WPWrapperETONYEK::checkInput(shared_ptr<librevenge::RVNGInputStream> &input)
{
	libetonyek::EtonyekDocument::Type type;
	libetonyek::EtonyekDocument::Confidence confidence=libetonyek::EtonyekDocument::isSupported(input.get(), &type);
#ifndef __EMSCRIPTEN__
	if (libetonyek::EtonyekDocument::CONFIDENCE_NONE == confidence)
	{
		// check for a directory
		if (!librevenge::RVNGDirectoryStream::isDirectory(m_options->getInput()))
			return false;
		if (m_fileType==Unknown)
		{
			// check for .key, .number, .pages extension, ie. we do not want to compress /
			bool findExtension=false;
			std::string name(m_options->getInput());
			size_t len=name.length();
			while (len>1 && name[len-1]=='/')
			{
				name=name.substr(0,len-1);
				--len;
			}
			for (int i=0; i<3; ++i)
			{
				char const *(extensions[3])= {".key", ".numbers", ".pages"};
				std::string extension=extensions[i];
				if (len>extension.length() && name.substr(len-extension.length())==extension)
				{
					findExtension=true;
					break;
				}
			}
			if (!findExtension)
				return false;
		}
		shared_ptr<librevenge::RVNGInputStream> tmpInput(new librevenge::RVNGDirectoryStream(m_options->getInput()));
		confidence=libetonyek::EtonyekDocument::isSupported(tmpInput.get(), &type);
		if (confidence==libetonyek::EtonyekDocument::CONFIDENCE_EXCELLENT && setType(type))
		{
			input=tmpInput;
			return true;
		}
		return false;
	}
	if (libetonyek::EtonyekDocument::CONFIDENCE_SUPPORTED_PART == confidence)
	{
		shared_ptr<librevenge::RVNGInputStream> tmpInput(librevenge::RVNGDirectoryStream::createForParent(m_options->getInput()));
		confidence=libetonyek::EtonyekDocument::isSupported(tmpInput.get(), &type);
		if (confidence==libetonyek::EtonyekDocument::CONFIDENCE_EXCELLENT && setType(type))
		{
			input=tmpInput;
			return true;
		}
		return false;
	}
#endif
	return confidence==libetonyek::EtonyekDocument::CONFIDENCE_EXCELLENT && setType(type);
}
bool WPWrapperETONYEK::setType(int type)
{
	Type fileType=Unknown;
	if (type==libetonyek::EtonyekDocument::TYPE_PAGES)
		fileType=Text;
	else if (type==libetonyek::EtonyekDocument::TYPE_KEYNOTE)
		fileType=Presentation;
	else if (type==libetonyek::EtonyekDocument::TYPE_NUMBERS)
		fileType=Spreadsheet;
	else
		return false;
	if (m_fileType != Unknown)
	{
		if (m_fileType!=fileType)
			return false;
		m_type=fileType;
	}
	m_type=fileType;
	return true;
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
