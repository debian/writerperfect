/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>

#include "OdfWrapperMSPUB.hxx"

#include <librevenge/librevenge.h>
#include <libodfgen/libodfgen.hxx>
#include <libmspub/libmspub.h>

#include "WPWrapperMSPUB.hxx"

#  ifdef ENABLE_EOT
extern "C"
{
#  include <libeot/libeot.h>
}
#  endif

namespace writerperfectrvngodf
{
#  ifdef ENABLE_EOT
static bool handleEmbeddedEOTFont(const librevenge::RVNGBinaryData &input, librevenge::RVNGBinaryData &output)
{
	EOTMetadata metadata;
	uint8_t *ttf = 0;
	unsigned ttfSize = 0;

	if (input.size() > std::numeric_limits<unsigned>::max()) // can't convert
		return false;

	const EOTError err = EOT2ttf_buffer(input.getDataBuffer(), unsigned(input.size()), &metadata, &ttf, &ttfSize);
	const bool success = err == EOT_SUCCESS;

	if (success)
	{
		output.clear();
		output.append(ttf, ttfSize);
	}

	EOTfreeMetadata(&metadata);
	EOTfreeBuffer(ttf);

	return success;
}
#  endif

OdfWrapperMSPUB::OdfWrapperMSPUB(shared_ptr<UserOptions> options) :
	OdfWrapper(shared_ptr<writerperfect::WPWrapper>(new writerperfect::WPWrapperMSPUB(options)))
{
}
bool OdfWrapperMSPUB::parseDocument(librevenge::RVNGInputStream &input, OdgGenerator &collector)
{
#  ifdef ENABLE_EOT
	collector.registerEmbeddedImageHandler("application/vnd.ms-fontobject", &handleEmbeddedEOTFont);
#  endif
	return OdfWrapper::parseDocument(input, collector);
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
