/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "OdfWrapperWPD.hxx"

#include <librevenge/librevenge.h>
#include <libodfgen/libodfgen.hxx>
#include <libwpd/libwpd.h>
#ifdef ENABLE_LIBWPG
#  include <libwpg/libwpg.h>
#endif

#include "WPWrapperWPD.hxx"

namespace writerperfectrvngodf
{
#  ifdef ENABLE_LIBWPG
static bool handleEmbeddedWPGObject(const librevenge::RVNGBinaryData &data, OdfDocumentHandler *pHandler,  const OdfStreamType streamType)
{
	OdgGenerator exporter;
	exporter.addDocumentHandler(pHandler, streamType);

	libwpg::WPGFileFormat fileFormat = libwpg::WPG_AUTODETECT;

	if (!libwpg::WPGraphics::isSupported(data.getDataStream()))
		fileFormat = libwpg::WPG_WPG1;

	return libwpg::WPGraphics::parse(data.getDataStream(), &exporter, fileFormat);
}

static bool handleEmbeddedWPGImage(const librevenge::RVNGBinaryData &input, librevenge::RVNGBinaryData &output)
{
	libwpg::WPGFileFormat fileFormat = libwpg::WPG_AUTODETECT;

	if (!libwpg::WPGraphics::isSupported(input.getDataStream()))
		fileFormat = libwpg::WPG_WPG1;

	librevenge::RVNGStringVector svgOutput;
	librevenge::RVNGSVGDrawingGenerator generator(svgOutput, "");
	return libwpg::WPGraphics::parse(input.getDataStream(), &generator, fileFormat) &&
	       OdfWrapper::convertSVGImage(svgOutput, output);
}
#  endif

OdfWrapperWPD::OdfWrapperWPD(shared_ptr<UserOptions> options) :
	OdfWrapper(shared_ptr<writerperfect::WPWrapper>(new writerperfect::WPWrapperWPD(options)))
{
}
bool OdfWrapperWPD::parseDocument(librevenge::RVNGInputStream &input, OdtGenerator &collector)
{
#  ifdef ENABLE_LIBWPG
	collector.registerEmbeddedObjectHandler("image/x-wpg", &handleEmbeddedWPGObject);
	collector.registerEmbeddedImageHandler("image/x-wpg", &handleEmbeddedWPGImage);
#  endif
	return OdfWrapper::parseDocument(input, collector);
}

}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
