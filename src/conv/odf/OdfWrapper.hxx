/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef ODF_WRAPPER_H
#define ODF_WRAPPER_H

#include <string>

#include <librevenge/librevenge.h>

#include "writerperfect_utils.hxx"

#include "WPWrapper.hxx"

class OdgGenerator;
class OdpGenerator;
class OdsGenerator;
class OdtGenerator;

namespace writerperfectrvngodf
{
/** a abstract wrapper to help convert file
 */
class OdfWrapper
{
public:
	//! constructor
	explicit OdfWrapper(shared_ptr<writerperfect::WPWrapper> wrapper) : m_wrapper(wrapper) {}
	//! destructor
	virtual ~OdfWrapper() {}
	//! return the wrapper type
	writerperfect::WPWrapper::Type getType() const;
	//! return true if an error is send
	bool isErrorSent() const;
	virtual bool checkInput(shared_ptr<librevenge::RVNGInputStream> &input);
	virtual bool parseDocument(librevenge::RVNGInputStream &input, OdgGenerator &collector);
	virtual bool parseDocument(librevenge::RVNGInputStream &input, OdpGenerator &collector);
	virtual bool parseDocument(librevenge::RVNGInputStream &input, OdsGenerator &collector);
	virtual bool parseDocument(librevenge::RVNGInputStream &input, OdtGenerator &collector);
	//! helper function which can be use to convert a output of svgdrawing to a svg image
	static bool convertSVGImage(librevenge::RVNGStringVector const &svgOutput, librevenge::RVNGBinaryData &output);
	//! the basic wrapper
	shared_ptr<writerperfect::WPWrapper> m_wrapper;
};
}
#endif

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
