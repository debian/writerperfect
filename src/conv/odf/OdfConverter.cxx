/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include <stdio.h>
#include <string.h>

#include "OdfConverter.hxx"

#include <libodfgen/libodfgen.hxx>

#include "OdfPackage.hxx"
#include "OdfWrapper.hxx"

#include "UsageHelper.hxx"
#include "UserOptions.hxx"

namespace writerperfectrvngodf
{
OdfConverter::OdfConverter(shared_ptr<UserOptions> options, std::string const &toolName, std::string const &usageMessage) :
	m_options(options), m_toolName(toolName), m_usageMessage(usageMessage)
{
}

OdfConverter::~OdfConverter()
{
}

int OdfConverter::printUsage() const
{
	if (m_toolName.empty() || m_usageMessage.empty())
	{
		fprintf(stderr, "INTERNAL ERROR: not enough data to show help.\n");
		return 1;
	}
	UsageHelper usage(m_toolName.c_str(), m_usageMessage.c_str());
	usage.addToDescription("If OUTPUT is omitted, the result is printed as Flat ODF to standard output.\n");
	if (m_options) m_options->addOptionsToUsageHelper(usage);
	return usage.printUsage();
}

int OdfConverter::printExtension(writerperfect::WPWrapper::Type type) const
{
	if (type == writerperfect::WPWrapper::Text)
		printf("odt\n");
	else if (type == writerperfect::WPWrapper::Drawing)
		printf("odg\n");
	else if (type == writerperfect::WPWrapper::Presentation)
		printf("odp\n");
	else if (type == writerperfect::WPWrapper::Spreadsheet)
		printf("ods\n");
	else
	{
		printf("unknown\n");
		return 1;
	}
	return 0;
}

bool OdfConverter::parseAndTreatOptions(int argc, char const *const *argv, int &exitCode) const
{
	if (!m_options)
	{
		fprintf(stderr, "INTERNAL ERROR: can not find the options.\n");
		exitCode=1;
	}
	else if (!m_options->parseOptions(argc, argv) || m_options->m_showHelp)
		exitCode=printUsage();
	else if (m_options->m_showListEncoding)
		exitCode=UsageHelper::printEncodings();
	else if (m_options->m_showVersion)
		exitCode=UsageHelper::printVersion(m_toolName.c_str());
	else if (!m_options->getInput())
		exitCode=printUsage();
	else
		return false;
	return true;
}

// basic wrapper
bool OdfConverter::convertDocument(writerperfect::WPWrapper &wrapper, librevenge::RVNGInputStream &input)
{
	if (!m_options)
		return false;
	writerperfect::WPWrapper::Type type=wrapper.m_type;
	if (type==writerperfect::WPWrapper::Drawing)
	{
		OdfPackage package(m_options->getOutput());
		OdgGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	if (type==writerperfect::WPWrapper::Presentation)
	{
		OdfPackage package(m_options->getOutput());
		OdpGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	if (type==writerperfect::WPWrapper::Spreadsheet)
	{
		OdfPackage package(m_options->getOutput());
		OdsGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	if (type==writerperfect::WPWrapper::Text)
	{
		OdfPackage package(m_options->getOutput());
		OdtGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	return false;
}

bool OdfConverter::isSupportedFormat(writerperfect::WPWrapper &wrapper, shared_ptr<librevenge::RVNGInputStream> &input)
{
	if (!m_options)
		return false;
	return input.get() && wrapper.checkInput(input);
}

int OdfConverter::convert(writerperfect::WPWrapper &wrapper, int argc, char const *const *argv)
{
	int exitCode;
	if (parseAndTreatOptions(argc, argv, exitCode))
		return exitCode;

	shared_ptr<librevenge::RVNGInputStream> input(new librevenge::RVNGFileStream(m_options->getInput()));
	if (!isSupportedFormat(wrapper,input))
	{
		if (m_options->m_showExtension)
			return printExtension(writerperfect::WPWrapper::Unknown);
		if (!wrapper.m_errorSent)
			fprintf(stderr, "ERROR: We have no confidence that you are giving us a valid document.\n");
		return 1;
	}
	if (m_options->m_showExtension)
		return printExtension(wrapper.m_type);
	if (!input || !convertDocument(wrapper, *input))
	{
		if (!wrapper.m_errorSent)
			fprintf(stderr, "ERROR : Couldn't convert the document\n");
		return 1;
	}

	return 0;
}

// odf wrapper
bool OdfConverter::convertDocument(OdfWrapper &wrapper, librevenge::RVNGInputStream &input)
{
	if (!m_options)
		return false;
	writerperfect::WPWrapper::Type type=wrapper.getType();
	if (type==writerperfect::WPWrapper::Drawing)
	{
		OdfPackage package(m_options->getOutput());
		OdgGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	if (type==writerperfect::WPWrapper::Presentation)
	{
		OdfPackage package(m_options->getOutput());
		OdpGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	if (type==writerperfect::WPWrapper::Spreadsheet)
	{
		OdfPackage package(m_options->getOutput());
		OdsGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	if (type==writerperfect::WPWrapper::Text)
	{
		OdfPackage package(m_options->getOutput());
		OdtGenerator collector;
		package.addHandlersTo(collector);
		return wrapper.parseDocument(input, collector) && package.write(collector);
	}
	return false;
}

bool OdfConverter::isSupportedFormat(OdfWrapper &wrapper, shared_ptr<librevenge::RVNGInputStream> &input)
{
	if (!m_options)
		return false;
	return input.get() && wrapper.checkInput(input);
}

int OdfConverter::convert(OdfWrapper &wrapper, int argc, char const *const *argv)
{
	int exitCode;
	if (parseAndTreatOptions(argc, argv, exitCode))
		return exitCode;

	shared_ptr<librevenge::RVNGInputStream> input(new librevenge::RVNGFileStream(m_options->getInput()));
	if (!isSupportedFormat(wrapper,input))
	{
		if (m_options->m_showExtension)
			return printExtension(writerperfect::WPWrapper::Unknown);
		if (!wrapper.isErrorSent())
			fprintf(stderr, "ERROR: We have no confidence that you are giving us a valid document.\n");
		return 1;
	}
	if (m_options->m_showExtension)
		return printExtension(wrapper.getType());
	if (!input || !convertDocument(wrapper, *input))
	{
		if (!wrapper.isErrorSent())
			fprintf(stderr, "ERROR : Couldn't convert the document\n");
		return 1;
	}

	return 0;
}

}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
