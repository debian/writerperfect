/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2006 Ariya Hidayat (ariya@kde.org)
 * Copyright (C) 2006-2007 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "WPWrapperCDR.hxx"
#include "OdfConverter.hxx"

#include "UserOptions.hxx"

#define TOOLNAME "cmx2odg"

using namespace writerperfectrvngodf;
int main(int argc, char *argv[])
{
	shared_ptr<UserOptions> options(new UserOptions);
	options->m_standartOuputOption=true;
	writerperfect::WPWrapperCDR wrapper(options, writerperfect::WPWrapperCDR::CDR_CMX);
	OdfConverter converter(options, TOOLNAME, "converts Corel Presentation Exchange documents to ODF.");
	return converter.convert(wrapper, argc, argv);
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
