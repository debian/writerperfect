/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include "OdfWrapperSTAROFFICE.hxx"

#include <librevenge/librevenge.h>
#include <libodfgen/libodfgen.hxx>
#include <libstaroffice/libstaroffice.hxx>

#include "WPWrapperSTAROFFICE.hxx"

namespace writerperfectrvngodf
{
static bool handleEmbeddedSTOFFSpreadsheetObject(const librevenge::RVNGBinaryData &data, OdfDocumentHandler *pHandler,  const OdfStreamType streamType);

static bool handleEmbeddedSTOFFGraphicObject(const librevenge::RVNGBinaryData &data, OdfDocumentHandler *pHandler,  const OdfStreamType streamType)
{
	OdgGenerator exporter;
	exporter.registerEmbeddedObjectHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicObject);
	exporter.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	exporter.addDocumentHandler(pHandler, streamType);
	return STOFFDocument::decodeGraphic(data, &exporter);
}
bool handleEmbeddedSTOFFSpreadsheetObject(const librevenge::RVNGBinaryData &data, OdfDocumentHandler *pHandler,  const OdfStreamType streamType)
{
	OdsGenerator exporter;
	exporter.registerEmbeddedObjectHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicObject);
	exporter.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	exporter.addDocumentHandler(pHandler, streamType);
	return STOFFDocument::decodeSpreadsheet(data, &exporter);
}

OdfWrapperSTAROFFICE::OdfWrapperSTAROFFICE(shared_ptr<UserOptions> options) :
	OdfWrapper(shared_ptr<writerperfect::WPWrapper>(new writerperfect::WPWrapperSTAROFFICE(options)))
{
}
bool OdfWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, OdgGenerator &collector)
{
	collector.registerEmbeddedObjectHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicObject);
	collector.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	return OdfWrapper::parseDocument(input, collector);
}
bool OdfWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, OdpGenerator &collector)
{
	collector.registerEmbeddedObjectHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicObject);
	collector.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	return OdfWrapper::parseDocument(input, collector);
}
bool OdfWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, OdsGenerator &collector)
{
	collector.registerEmbeddedObjectHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicObject);
	collector.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	return OdfWrapper::parseDocument(input, collector);
}
bool OdfWrapperSTAROFFICE::parseDocument(librevenge::RVNGInputStream &input, OdtGenerator &collector)
{
	collector.registerEmbeddedObjectHandler("image/stoff-odg", &handleEmbeddedSTOFFGraphicObject);
	collector.registerEmbeddedObjectHandler("image/stoff-ods", &handleEmbeddedSTOFFSpreadsheetObject);
	return OdfWrapper::parseDocument(input, collector);
}
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
