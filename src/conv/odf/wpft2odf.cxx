/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string.h>

#include <librevenge-stream/librevenge-stream.h>
#include <libodfgen/libodfgen.hxx>

#include "OdfConverter.hxx"
#include "OdfWrapper.hxx"
#include "UserOptions.hxx"
#include "WPWrapper.hxx"

#ifdef ENABLE_LIBABW
#  include "WPWrapperABW.hxx"
#endif
#ifdef ENABLE_LIBCDR
#  include "WPWrapperCDR.hxx"
#endif
#ifdef ENABLE_LIBEBOOK
#  include "WPWrapperEBOOK.hxx"
#endif
#ifdef ENABLE_LIBETONYEK
#  include "WPWrapperETONYEK.hxx"
#endif
#ifdef ENABLE_LIBFREEHAND
#  include "WPWrapperFREEHAND.hxx"
#endif
#ifdef ENABLE_LIBMSPUB
#  include "OdfWrapperMSPUB.hxx"
#endif
#ifdef ENABLE_LIBMWAW
#  include "OdfWrapperMWAW.hxx"
#endif
#ifdef ENABLE_LIBPAGEMAKER
#  include "WPWrapperPAGEMAKER.hxx"
#endif
#ifdef ENABLE_LIBSTAROFFICE
#  include "OdfWrapperSTAROFFICE.hxx"
#endif
#ifdef ENABLE_LIBVISIO
#  include "WPWrapperVISIO.hxx"
#endif
#ifdef ENABLE_LIBWPD
#  include "OdfWrapperWPD.hxx"
#endif
#ifdef ENABLE_LIBWPG
#  include "WPWrapperWPG.hxx"
#endif
#ifdef ENABLE_LIBWPS
#  include "OdfWrapperWPS.hxx"
#endif
#ifdef ENABLE_LIBZMF
#  include "WPWrapperZMF.hxx"
#endif

#define TOOLNAME "wpft2odf"

#define CHECK_CONVERTER_LIB(x)															\
	do {																				\
		if (!wpWrapper && !wrapper) {												    \
            shared_ptr<writerperfect::WPWrapper> testWrapper(new writerperfect::WPWrapper##x(options)); \
			if (converter.isSupportedFormat(*testWrapper,input))						\
				wpWrapper=testWrapper;													\
		}																				\
	} while(0)
#define CHECK_CONVERTER_LIB2(x)															\
	do {																				\
		if (!wpWrapper && !wrapper) {												    \
			shared_ptr<OdfWrapper> testWrapper(new OdfWrapper##x(options)); 			\
			if (converter.isSupportedFormat(*testWrapper,input))						\
				wrapper=testWrapper;													\
		}																				\
	} while(0)

using namespace writerperfectrvngodf;
int main(int argc, char *argv[])
{
	shared_ptr<UserOptions> options(new UserOptions);
	options->m_encodingOption=options->m_extensionOption
	                          =options->m_passwordOption=options->m_standartOuputOption=true;
	int exitCode;
	OdfConverter converter(options, TOOLNAME, "converts documents to ODF.");
	if (converter.parseAndTreatOptions(argc, argv, exitCode))
		return exitCode;

	shared_ptr<librevenge::RVNGInputStream> input(new librevenge::RVNGFileStream(options->getInput()));
	shared_ptr<writerperfect::WPWrapper> wpWrapper;
	shared_ptr<OdfWrapper> wrapper;
#ifdef ENABLE_LIBABW
	CHECK_CONVERTER_LIB(ABW);
#endif
#ifdef ENABLE_LIBCDR
	CHECK_CONVERTER_LIB(CDR);
#endif
#ifdef ENABLE_LIBEBOOK
	CHECK_CONVERTER_LIB(EBOOK);
#endif
#ifdef ENABLE_LIBETONYEK
	CHECK_CONVERTER_LIB(ETONYEK);
#endif
#ifdef ENABLE_LIBFREEHAND
	CHECK_CONVERTER_LIB(FREEHAND);
#endif
#ifdef ENABLE_LIBMSPUB
	CHECK_CONVERTER_LIB2(MSPUB);
#endif
#ifdef ENABLE_LIBMWAW
	CHECK_CONVERTER_LIB2(MWAW);
#endif
#ifdef ENABLE_LIBPAGEMAKER
	CHECK_CONVERTER_LIB(PAGEMAKER);
#endif
#ifdef ENABLE_LIBSTAROFFICE
	CHECK_CONVERTER_LIB2(STAROFFICE);
#endif
#ifdef ENABLE_LIBVISIO
	CHECK_CONVERTER_LIB(VISIO);
#endif
#ifdef ENABLE_LIBWPD
	CHECK_CONVERTER_LIB2(WPD);
#endif
#ifdef ENABLE_LIBWPG
	CHECK_CONVERTER_LIB(WPG);
#endif
#ifdef ENABLE_LIBWPS
	CHECK_CONVERTER_LIB2(WPS);
#endif
#ifdef ENABLE_LIBZMF
	CHECK_CONVERTER_LIB(ZMF);
#endif
	if (!wpWrapper && !wrapper)
	{
		if (options->m_showExtension)
			return converter.printExtension(writerperfect::WPWrapper::Unknown);
		return 1;
	}
	if (options->m_showExtension)
		return wrapper ? converter.printExtension(wrapper->getType()) : converter.printExtension(wpWrapper->m_type);
	if (wrapper && converter.convertDocument(*wrapper, *input))
		return 0;
	if (wpWrapper && converter.convertDocument(*wpWrapper, *input))
		return 0;
	fprintf(stderr, "ERROR : Couldn't convert the document\n");
	return 1;
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
