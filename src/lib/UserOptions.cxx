/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include "writerperfect_utils.hxx"

#include "UsageHelper.hxx"
#include "UserOptions.hxx"

#include <iostream>

UserOptions::UserOptions()
	: m_encodingOption(false)
	, m_passwordOption(false)
	, m_extensionOption(false)
	, m_standartOuputOption(false)
	, m_versionOption(false)
	, m_splitOption(false)
	, m_stylesOption(false)
	, m_layoutOption(false)

	, m_showExtension(false)
	, m_showHelp(false)
	, m_showListEncoding(false)
	, m_showVersion(false)

	, m_input("")
	, m_output("")

	, m_encoding("")
	, m_password(0)

	, m_standartOutput(false)

	, m_version(0)
	, m_split(0)
	, m_styles(0)
	, m_layout(0)
{
}

UserOptions::UserOptions(UserOptions const &orig)
	: m_encodingOption(orig.m_encodingOption)
	, m_passwordOption(orig.m_passwordOption)
	, m_extensionOption(orig.m_extensionOption)
	, m_standartOuputOption(orig.m_standartOuputOption)
	, m_versionOption(orig.m_versionOption)
	, m_splitOption(orig.m_splitOption)
	, m_stylesOption(orig.m_stylesOption)
	, m_layoutOption(orig.m_layoutOption)

	, m_showExtension(orig.m_showExtension)
	, m_showHelp(orig.m_showHelp)
	, m_showListEncoding(orig.m_showListEncoding)
	, m_showVersion(orig.m_showVersion)

	, m_input(orig.m_input)
	, m_output(orig.m_output)

	, m_encoding(orig.m_encoding)
	, m_password(orig.m_password)

	, m_standartOutput(orig.m_standartOutput)

	, m_version(orig.m_version)
	, m_split(orig.m_split)
	, m_styles(orig.m_styles)
	, m_layout(orig.m_layout)
{
}

UserOptions::~UserOptions()
{
}

bool UserOptions::parseOptions(int argc, char const *const *argv)
{
	for (int i = 1; i < argc; i++)
	{
		if (!parseAnOption(i, argc, argv))
			return false;
	}
	if (m_standartOutput) m_output.clear();
	return true;
}

bool UserOptions::parseAnOption(int &i, int argc, char const *const *argv)
{
	if (m_passwordOption && strcmp(argv[i], "--password")==0 && i < argc - 1)
		m_password = argv[++i];
	else if (m_passwordOption && strncmp(argv[i], "--password=", 11)==0)
		m_password = &argv[i][11];
	else if (m_standartOuputOption && strcmp(argv[i], "--stdout")==0)
		m_standartOutput = true;
	else if (strcmp(argv[i], "--help")==0)
		m_showHelp=true;
	else if (strcmp(argv[i], "--version")==0)
		m_showVersion=true;
	else if (m_extensionOption && (strcmp(argv[i], "--extension")==0 || strcmp(argv[i], "-x")==0))
		m_showExtension = true;
	else if (m_encodingOption && strcmp(argv[i], "--encoding")==0 && i < argc - 1)
		m_encoding = argv[++i];
	else if (m_encodingOption && strcmp(argv[i], "--list-encodings")==0)
		m_showListEncoding = true;
	else if (m_versionOption && strncmp(argv[i], "--version=", 10)==0)
		m_version = std::stoi(&argv[i][10]);
	else if (m_splitOption && strncmp(argv[i], "--split=", 8)==0)
	{
		std::string value = std::string(&argv[i][8]);
		if (value == "page-break")
			m_split = 0;
		else if (value == "heading")
			m_split = 1;
		else if (value == "size")
			m_split = 2;
		else if (value == "none")
			m_split = 3;
		else if (value == "detect")
			m_split = 4;
		else
			return false;
	}
	else if (m_stylesOption && strncmp(argv[i], "--styles=", 9)==0)
	{
		std::string value = std::string(&argv[i][9]);
		if (value == "css")
			m_styles = 0;
		else if (value == "inline")
			m_styles = 1;
		else
			return false;
	}
	else if (m_layoutOption && strncmp(argv[i], "--layout=", 9)==0)
	{
		std::string value = std::string(&argv[i][9]);
		if (value == "reflowable")
			m_layout = 0;
		else if (value == "fixed")
			m_layout = 1;
		else
			return false;
	}
	else if (strncmp(argv[i], "--", 2)==0)
		return false;
	else if (m_input.empty())
		m_input = argv[i];
	else if (!m_input.empty() && m_output.empty())
		m_output = argv[i];
	else
		return false;
	return true;
}

void UserOptions::addOptionsToUsageHelper(UsageHelper &helper) const
{
	if (m_encodingOption)
	{
		helper.addToOptions("\t--encoding ENCODING\tset the INPUT encoding. Use --list-encodings\n");
		helper.addToOptions("\t\t\t\tto see which encodings can be used.\n");
		helper.addToOptions("\t--list-encodings\tshow the available encodings and exit\n");
	}
	if (m_passwordOption)
		helper.addToOptions("\t--password PASSWORD\tset password to open the file\n");
	if (m_standartOuputOption)
		helper.	addToOptions("\t--stdout\t\tprint the result as flat XML to standard output\n");
	if (m_extensionOption)
		helper.addToOptions("\t-x, --extension\t\tprint extension for output (odt, ods, odp, odg)\n");
}
/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
