/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#include <stdio.h>

#include "WPWrapper.hxx"

#include "UserOptions.hxx"

namespace writerperfect
{
WPWrapper::WPWrapper(shared_ptr<UserOptions> options)  :
	m_type(WPWrapper::Unknown), m_options(options), m_errorSent(false)
{
}

WPWrapper::~WPWrapper()
{
}

bool WPWrapper::checkInput(shared_ptr<librevenge::RVNGInputStream> &)
{
	fprintf(stderr, "INTERNAL ERROR: WPWrapper::checkInput must not be called.\n");
	m_errorSent=true;
	return false;
}

bool WPWrapper::parseDocument(librevenge::RVNGInputStream &, librevenge::RVNGDrawingInterface &)
{
	fprintf(stderr, "INTERNAL ERROR: WPWrapper::parseDocument must not be called.\n");
	m_errorSent=true;
	return false;
}

bool WPWrapper::parseDocument(librevenge::RVNGInputStream &, librevenge::RVNGPresentationInterface &)
{
	fprintf(stderr, "INTERNAL ERROR: WPWrapper::parseDocument must not be called.\n");
	m_errorSent=true;
	return false;
}

bool WPWrapper::parseDocument(librevenge::RVNGInputStream &, librevenge::RVNGSpreadsheetInterface &)
{
	fprintf(stderr, "INTERNAL ERROR: WPWrapper::parseDocument must not be called.\n");
	m_errorSent=true;
	return false;
}

bool WPWrapper::parseDocument(librevenge::RVNGInputStream &, librevenge::RVNGTextInterface &)
{
	fprintf(stderr, "INTERNAL ERROR: WPWrapper::parseDocument must not be called.\n");
	m_errorSent=true;
	return false;
}

}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
