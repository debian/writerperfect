/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* libwpd
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef WP_WRAPPER_H
#define WP_WRAPPER_H

#include <string>

#include <librevenge/librevenge.h>

#include "writerperfect_utils.hxx"

class UserOptions;

namespace writerperfect
{
/** a abstract class to create import library wrapper
 */
class WPWrapper
{
public:
	enum Type { Unknown, Drawing, Presentation, Spreadsheet, Text };
	//! constructor
	explicit WPWrapper(shared_ptr<UserOptions> options);
	//! destructor
	virtual ~WPWrapper();
	//! checks if the input stream corresponds to a know format and updated the file type
	virtual bool checkInput(shared_ptr<librevenge::RVNGInputStream> &input) = 0;
	//! tries to parse a graphic document
	virtual bool parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGDrawingInterface &collector);
	//! tries to parse a presentation document
	virtual bool parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGPresentationInterface &collector);
	//! tries to parse a spreadsheet document
	virtual bool parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGSpreadsheetInterface &collector);
	//! tries to parse a text document
	virtual bool parseDocument(librevenge::RVNGInputStream &input, librevenge::RVNGTextInterface &collector);

	//! the type
	Type m_type;
	//! the option
	shared_ptr<UserOptions> m_options;
	//! flag to know if an error was already print
	bool m_errorSent;
};
}
#endif

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
