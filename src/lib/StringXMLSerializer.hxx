/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Major Contributor(s):
 * Copyright (C) 2002-2004 William Lachance (wrlach@gmail.com)
 * Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
 *
 * For minor contributions see the git repository.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef INCLUDED_STRING_XML_SERIALIZER_HXX
#define INCLUDED_STRING_XML_SERIALIZER_HXX

#include <librevenge/librevenge.h>

class StringXMLSerializer
{
public:
	StringXMLSerializer();

	const librevenge::RVNGString &getData() const
	{
		return m_data;
	}

	void setDocType(const char *docType);

	void startDocument();
	void endDocument();
	void startElement(const char *psName, const librevenge::RVNGPropertyList &xPropList);
	void endElement(const char *psName);
	void characters(const librevenge::RVNGString &sCharacters);

private:
	librevenge::RVNGString m_data;
	bool m_isDocumentStarted;
	bool m_isTagOpened;
	librevenge::RVNGString m_openedTagName;
};

#endif

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
