/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef USAGEHELPER_HXX
#define USAGEHELPER_HXX

#include <sstream>
#include <string>

class UsageHelper
{
public:
	UsageHelper(const char *name, const char *desc, const char *opts = "[OPTIONS] INPUT [OUTPUT]");

	int printUsage() const;
	static int printVersion(const char *name);
	static int printEncodings();

	void addToDescription(const char *line);
	void addToOptions(const char *line);

private:
	const std::string m_name;
	std::ostringstream m_desc;
	std::ostringstream m_usage;
	std::ostringstream m_options;
};

#endif

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
