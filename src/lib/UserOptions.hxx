/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifndef USEROPTIONS_HXX
#define USEROPTIONS_HXX

#include <sstream>
#include <string>

class UsageHelper;

class UserOptions
{
public:
	UserOptions();
	virtual ~UserOptions();

	virtual void addOptionsToUsageHelper(UsageHelper &helper) const;
	bool parseOptions(int argc, char const *const *argv);

	char const *getInput() const
	{
		return m_input.empty() ? 0 : m_input.c_str();
	}
	char const *getOutput() const
	{
		return m_output.empty() ? 0 : m_output.c_str();
	}
	void setOutput(std::string const &output)
	{
		m_output=output;
	}
	char const *getEncoding() const
	{
		return m_encoding.empty() ? 0 : m_encoding.c_str();
	}
	char const *getPassword() const
	{
		return m_password;
	}
	int getVersion() const
	{
		return m_version;
	}
	int getSplit() const
	{
		return m_split;
	}
	int getStyles() const
	{
		return m_styles;
	}
	int getLayout() const
	{
		return m_layout;
	}

	bool m_encodingOption, m_passwordOption, m_extensionOption, m_standartOuputOption;

	bool m_versionOption;

	bool m_splitOption;

	bool m_stylesOption;

	bool m_layoutOption;

	bool m_showExtension, m_showHelp, m_showListEncoding, m_showVersion;

protected:
	virtual bool parseAnOption(int &i, int argc, char const *const *argv);
	explicit UserOptions(UserOptions const &orig);

	std::string m_input;
	std::string m_output;
	std::string m_encoding;
	char const *m_password;

	bool m_standartOutput;

	/// Output version.
	int m_version;

	/// Output split type.
	int m_split;

	/// Output styles type.
	int m_styles;

	/// Output layout type.
	int m_layout;

private:
	UserOptions &operator=(UserOptions const &orig);
};

#endif

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
