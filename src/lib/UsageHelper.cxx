/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/* writerperfect
 * Version: MPL 2.0 / LGPLv2.1+
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU Lesser General Public License Version 2.1 or later
 * (LGPLv2.1+), in which case the provisions of the LGPLv2.1+ are
 * applicable instead of those above.
 *
 * For further information visit http://libwpd.sourceforge.net
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "UsageHelper.hxx"
#include "writerperfect_utils.hxx"

#include <iostream>

#ifndef VERSION
#define VERSION "unknown version"
#endif

namespace
{

const char *ENCODINGS[] =
{
	"CP037",
	"CP1006",
	"CP1026",
	"CP1250",
	"CP1251",
	"CP1252",
	"CP1253",
	"CP1254",
	"CP1255",
	"CP1256",
	"CP1257",
	"CP1258",
	"CP424",
	"CP437",
	"CP500",
	"CP737",
	"CP775",
	"CP850",
	"CP852",
	"CP855",
	"CP856",
	"CP857",
	"CP860",
	"CP861",
	"CP862",
	"CP863",
	"CP864",
	"CP865",
	"CP866",
	"CP869",
	"CP874",
	"CP875",
	"MacArabic",
	"MacCEurope",
	"MacCeltic",
	"MacCroation",
	"MacCyrillic",
	"MacDevanage",
	"MacFarsi",
	"MacGaelic",
	"MacGreek",
	"MacGujarati",
	"MacGurmukhi",
	"MacHebrew",
	"MacIceland",
	"MacInuit",
	"MacRoman",
	"MacRomanian",
	"MacThai",
	"MacTurkish"
};

}

UsageHelper::UsageHelper(const char *const name, const char *const desc, const char *const opts)
	: m_name(name)
	, m_desc()
	, m_usage()
	, m_options()
{
	m_desc << '`' << name << "\' " << desc << '\n';
	m_usage << "Usage: " << name << ' ' << opts << '\n';
	m_options
	        << "Options:\n"
	        << "\t--help\t\t\tshow this help message\n"
	        << "\t--version\t\tprint version and exit\n"
	        ;
}

int UsageHelper::printUsage() const
{
	std::cout
	        << m_desc.str() << '\n'
	        << m_usage.str() << '\n'
	        << m_options.str() << '\n'
	        << "Report bugs to <https://sourceforge.net/p/libwpd/tickets/>.\n"
	        ;
	return 0;
}

int UsageHelper::printVersion(const char *const name)
{
	std::cout << name << " " VERSION << '\n';
	return 0;
}

int UsageHelper::printEncodings()
{
	for (size_t i = 0; i != WPFT_NUM_ELEMENTS(ENCODINGS); ++i)
		std::cout << ENCODINGS[i] << '\n';
	return 0;
}

void UsageHelper::addToDescription(const char *const line)
{
	m_desc << line;
}

void UsageHelper::addToOptions(const char *const line)
{
	m_options << line;
}

/* vim:set shiftwidth=4 softtabstop=4 noexpandtab: */
