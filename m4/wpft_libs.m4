# wpft_CHECK_LIB(name,lib-name,lib-version,help-str,[default-value],[type-var])
#
# Checks for presence of a library lib-name using PKG_CHECK_MODULES.
# If successful, sets have_$lib-name and exports USE_LIB$name
# automake conditional.
# ------------------------------------------------------------------------------------------
AC_DEFUN([wpft_CHECK_LIB],
[dnl
AC_ARG_WITH($2,
    [AS_HELP_STRING([--with-$2], [$4])],
    [with_$2="$withval"],
    [with_$2=m4_default([$5], [auto])]
)
AS_IF([test "x$with_$2" != "xno"], [
        PKG_CHECK_MODULES([$1], [$3],
            [
                have_$2=yes
                ifelse([$6], [], [], [$6=yes])
            ],
            [have_$2=no])
    ], [have_$2=no])

AM_CONDITIONAL([USE_LIB$1], [test "x$have_$2" = "xyes"])
AS_IF([test "x$have_$2" = "xno" -a "x$with_$2" = "xyes"], [
    AC_MSG_ERROR([$2 requested but not found])
]) dnl
]) # wpft_CHECK_LIB

# wpft_CHECK_IMPORT_LIB(name,lib-name,lib-version,help-str)
#
# Checks for presence of import library lib-name using PKG_CHECK_MODULES.
# If successful, sets have_$lib-name, have_import_lib and exports USE_LIB$name
# automake conditional.
#
# Uses with_import_libs.
# ------------------------------------------------------------------------------------------
AC_DEFUN([wpft_CHECK_IMPORT_LIB],
[dnl
wpft_CHECK_LIB([$1],[$2],[$3],[$4],[$with_import_libs],[have_import_lib])
AS_IF([test "x$have_$2" = "xyes"], [ $1_CFLAGS+=" -DENABLE_LIB$1" ])
]) # wpft_CHECK_IMPORT_LIB

# wpft_CHECK_EXPORT_LIB(name,lib-name,lib-version,help-str)
#
# Checks for presence of export library lib-name using PKG_CHECK_MODULES.
# If successful, sets have_$lib-name, have_export_lib and exports USE_LIB$name
# automake conditional.
#
# Uses with_export_libs.
# ------------------------------------------------------------------------------------------
AC_DEFUN([wpft_CHECK_EXPORT_LIB],
[dnl
wpft_CHECK_LIB([$1],[$2],[$3],[$4],[$with_export_libs],[have_export_lib])
]) # wpft_CHECK_IMPORT_LIB

dnl vim:set shiftwidth=4 softtabstop=4 expandtab:
